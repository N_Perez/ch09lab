/*
Nicholas Perez &  Seok Hyun Kim  COSC 2336-302
Chapter 9 Lab 1 - Search
*/



#include <iostream>
#include "rollodex.h"
#include <iomanip>

using namespace std;

int main()
{
	Rollodex fillo;
	fillo.fileIn("records.txt");
	fillo.searchSequential("Kirisame, Marissa B");
	fillo.searchSequential("Kirisame, Marisa B");
	fillo.searchSequential("Kirari Moroboshi");
	fillo.searchSequential("Ordinary Magician");
	fillo.searchSequential("Harrison, Samuel C");
	system("pause");
	return 0;
}